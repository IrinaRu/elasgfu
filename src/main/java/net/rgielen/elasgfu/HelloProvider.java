package net.rgielen.elasgfu;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloProvider {

    final String helloVal;

    public HelloProvider(String helloVal) {
        this.helloVal = helloVal;
    }

    public HelloProvider() {
        this("Hello");
    }

    public String getHelloVal() {
        return helloVal;
    }
}
