package net.rgielen.elasgfu.todo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import javax.sql.DataSource;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TodoConfig.class)
public class TodoConfigTest {

    @Inject
    private DataSource dataSource;

    @Test
    public void testSpringCanInjectDataSource() throws Exception {
        assertNotNull(dataSource);
    }
}